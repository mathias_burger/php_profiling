
Start des HTML-Servers:  
- Wechsel per Konsole auf das Verzeichnis  
- Start des Servers mit dem Konsolenbefehl:
   python2 -m SimpleHTTPServer 8081
  or
   python3 -m http.server 8081
- Aufruf der Seite im Browser über 'http://localhost:8081'  
